#!/bin/bash
echo -e "Wait meanwhile we prepar the system to install the packeges...\n"
sudo apt update >> /dev/null 2>&1 
sudo apt install -y net-tools wireless-tools network-manager >> /dev/null 2>&1

echo "#                                                         "
echo "#                                                         "
echo "#   __        _____ _____ ___ ____ ___  _   __    _       "
echo "#   \ \      / /_ _|  ___|_ _/ ___/ _ \| \ | | \ | |      "
echo "#    \ \ /\ / / | || |_   | | |  | | | |  \| |  \| |      "
echo "#     \ V  V /  | ||  _|  | | |__| |_| | |\  | |\  |      "
echo "#      \_/\_/  |___|_|   |___\____\___/|_| \_|_| \_|      "
echo "#                                                         "
echo "#                                                         "
echo "#                                                 by Dave "
echo -e "\n"
echo -e "Insert SSID to connect:\n\n"


ifconfig wlan0 up
nmcli dev wifi

#echo -e "\n"

echo "SSID: " ; read SSID
echo "Insert password: "; read -s SENHA


rm -rf /etc/wpa_supplicant.conf
rm -rf /etc/systemd/system/wifi
rm -rf /etc/systemd/system/wifi.service
rm -rf /etc/systemd/system/multi-user.target.wants/wifi.service

# Writting script file
touch /etc/systemd/system/wifi.service
touch /etc/systemd/system/wifi

echo "#!/bin/bash">> /etc/systemd/system/wifi
echo "### BEGIN INIT INFO" >> /etc/systemd/system/wifi
echo "# Provides:          wifi" >> /etc/systemd/system/wifi
echo "# Required-Start:    $all" >> /etc/systemd/system/wifi
echo "# Required-Stop:" >> /etc/systemd/system/wifi
echo "# Default-Start:     2 3 4 5" >> /etc/systemd/system/wifi
echo "# Default-Stop:" >> /etc/systemd/system/wifi
echo "# Short-Description: Wifi Connection by Dave" >> /etc/systemd/system/wifi
echo "### END INIT INFO" >> /etc/systemd/system/wifi

echo "ifconfig wlan0 up" >> /etc/systemd/system/wifi
echo "wpa_passphrase $SSID $SENHA >> /etc/wpa_supplicant.conf" >> /etc/systemd/system/wifi
echo "wpa_supplicant -iwlan0 -B -c/etc/wpa_supplicant.conf" >> /etc/systemd/system/wifi
echo "dhclient wlan0" >> /etc/systemd/system/wifi

#Writting service file

echo "[Unit]" >> /etc/systemd/system/wifi.service
echo "Description=Wifi Connection by Dave" >> /etc/systemd/system/wifi.service
echo "After=network.target" >> /etc/systemd/system/wifi.service
echo "StartLimitIntervalSec=0" >> /etc/systemd/system/wifi.service

echo "[Service]" >> /etc/systemd/system/wifi.service

echo "Type=simple" >> /etc/systemd/system/wifi.service
echo "Restart=always" >> /etc/systemd/system/wifi.service
echo "RemainAfterExit=yes" >> /etc/systemd/system/wifi.service
echo "ExecStart=/etc/systemd/system/wifi start" >> /etc/systemd/system/wifi.service
echo "ExecStop=/etc/systemd/system/wifi stop" >> /etc/systemd/system/wifi.service
echo "ExecReload=/etc/systemd/system/wifi restart" >> /etc/systemd/system/wifi.service

echo "[Install]" >> /etc/systemd/system/wifi.service
echo "WantedBy=Defaults.target" >> /etc/systemd/system/wifi.service

cp /etc/systemd/system/wifi.service /usr/lib/systemd/system/

ln -s '/usr/lib/systemd/system/wifi.service' '/etc/systemd/system/multi-user.target.wants/wifi.service'

chmod +x /etc/systemd/system/wifi
chmod 664 /usr/lib/systemd/system/wifi.service

/etc/systemd/system/wifi >> /dev/null 2>&1

systemctl daemon-reload
systemctl enable wifi

echo "Connection Successfully at "$SSID"."

exit 0
